<?php

class calcController{

    public $stage = 0;
    public $action = 'stage';


    public function init(){
        //дергаем шаг
        if(isset($_POST['stage'])){
            $this->stage = $_POST['stage'];
        } else {
            $this->stage = 0;
        }

        if(isset($_POST['action'])){
            $this->action = $_POST['action'];
        } else {
            $this->action = 'stage';
        }

        switch ($this->action) {
            case 'stage':
                $this->actionStage();
                break;
            case 'result':
                $this->actionResult();
                break;
            default:
                $this->actionStage();
        }
    }

    public function actionStage(){

        require_once('/home/u687100369/public_html/modules/calc/view/calcController/stage.php');
        require_once('/home/u687100369/public_html/modules/calc/models/calcModel.php');

        $model = new calcModel();

        if(
        isset($_POST['previous'])
        ){
            $this->stage -= 1;
            $data = $model->getData($this->stage);
            $data['result'] = '';

            $view = new stageView();
            $view->render(
                $data
            );
        }

        if($this->stage == '1'){
            if(
                !isset($_POST['P1']) ||
                !isset($_POST['T1']) ||
                !isset($_POST['n1']) ||
                !isset($_POST['u']) ||
                !isset($_POST['KF'])
            ){
                $data = $model->getData($this->stage);
                $data['result'] = '';

                $view = new stageView();
                $view->render(
                    $data
                );
            }

            if(
                isset($_POST['next'])
            ){
                $result = $_POST['P1'] + $_POST['T1'] + $_POST['n1'] + $_POST['u'] + $_POST['KF'];

                $this->stage += 1;
                $data = $model->getData($this->stage);
                $data['result'] = $result;

                $view = new stageView();
                $view->render(
                    $data
                );
            } else {
                $data = $model->getData($this->stage);
                $data['result'] = '';

                $view = new stageView();
                $view->render(
                    $data
                );
            };

        }

        if($this->stage == '2'){

            if(
                !isset($_POST['Ksi']) ||
                !isset($_POST['d1']) ||
                !isset($_POST['d2']) ||
                !isset($_POST['remen']) ||
                !isset($_POST['result'])
            ){
                $data = $model->getData($this->stage);
                $data['result'] = '';

                $view = new stageView();
                $view->render(
                    $data
                );
            }

            if(
            isset($_POST['next'])
            ){
                $result = $_POST['P1'] + $_POST['T1'] + $_POST['n1'] + $_POST['u'] + $_POST['KF'];

                $this->stage += 1;
                $data = $model->getData($this->stage);
                $data['result'] = $result;

                $view = new stageView();
                $view->render(
                    $data
                );
            } else {
                $data = $model->getData($this->stage);
                $data['result'] = '';

                $view = new stageView();
                $view->render(
                    $data
                );
            };

        }

        if($this->stage == '3'){
            if(
                !isset($_POST['result'])
            ){
                $data = $model->getData($this->stage);
                $data['result'] = '';

                $view = new stageView();
                $view->render(
                    $data
                );
            }

            if(
            isset($_POST['next'])
            ){
                $result = $_POST['P1'] + $_POST['T1'] + $_POST['n1'] + $_POST['u'] + $_POST['KF'];

                $this->stage += 1;
                $data = $model->getData($this->stage);
                $data['result'] = $result;

                $view = new stageView();
                $view->render(
                    $data
                );
            } else {
                $data = $model->getData($this->stage);
                $data['result'] = '';

                $view = new stageView();
                $view->render(
                    $data
                );
            };

        }

        $this->stage = 1;
        $data = $model->getData($this->stage);
        $data['result'] = '';

        $view = new stageView();
        $view->render(
            $data
        );
    }


    public function actionResult(){
        echo 'result';
       return false;
    }


};

?>