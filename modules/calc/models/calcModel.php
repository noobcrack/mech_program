<?php class calcModel {
    public function getData($stage){
        switch ($stage) {
            case '1':
                return array(
                    'stage' => '1',
                    'title' => 'Исходные данные',
                    'description' => '<p>В зависимости от формы сечения ремни бывают:
                        плоские, круглые, клиновые и поликлиновые, зубчатые.
                        Наибольшее практическое применение в приводах имеют клиноременные передачи,
                        расчет которых рассмотрен в данной расчетной программе.</p>',
                    'table' => '
                <table>
                        <tr>
                            <td></td>
                            <td>Мощность на ведущем шкиве</td>
                            <td>P<sub>1</sub></td>
                            <td>кВт</td>
                            <td><input type="number" name="P1" /></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Крутящий момент на ведущем шкиве</td>
                            <td>T<sub>1</sub></td>
                            <td>Н*м</td>
                            <td><input type="number" name="T1" /></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Частота вращения ведущего шкива</td>
                            <td>n<sub>1</sub></td>
                            <td>об/мин</td>
                            <td><input type="number" name="n1" /></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Передаточное отношение</td>
                            <td>u</td>
                            <td></td>
                            <td><input type="number" name="u" /></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Коэффициент динамичности</td>
                            <td>K<sub>F</sub></td>
                            <td></td>
                            <td><input type="number" name="KF" /></td>
                        </tr>
                    </table>',
                );
                break;
            case '2':
                return array(
                    'stage' => '2',
                    'title' => 'Выбор типа ремня и определение диаметров шкивов',
                    'description' => '
                        <p>
                            По крутящему моменту T<sub>1</sub> на быстроходном валу,
                            в соответствии с таблицами 1 и 2,
                            выбирают тип ремня и назначают диаметр малого шкива.
                        </p>
                        <p>
                            Затем определяют диаметр большего шкива d<sub>2</sub>, мм.
                        </p>
                        <p>
                            <img src="img/formula/1.gif" />
                        </p>
                        <p>
                            Полученное значение диаметра d<sub>2</sub> согласуют со стандартным значением из ряда:
                            63, 80, 100, 125,140, 160, 180, 200, 225, 250, 280, 315, 355, 400,450, 500, 560, 630, 710, 800
                        </p>
                        <p>
                            При этом стандартный диаметр d<sub>2</sub> не должен отличаться более,
                            чем на 4% от расчетного; в противном случае необходимо оставить расчетный диаметр d<sub>2</sub>.
                        </p>',
                    'table' => '
                    <table>
                        <tr>
                            <td>По таблице 1</td>
                            <td>
                                Диаметр малого шкива
                            </td>
                            <td>
                                d<sub>1</sub>
                            </td>
                            <td>
                                мм
                            </td>
                            <td>
                                <input type="number" name="d1" />
                            </td>
                        </tr>
                        <tr>
                            <td><!----></td>
                            <td>
                                Коэффициент относительного скольжения (0.01-0.02)
                            </td>
                            <td>
                                &xi;
                            </td>
                            <td><!----></td>
                            <td>
                                <input type="number" name="Ksi" />
                            </td>
                        </tr>
                        <tr>
                            <td>По таблице 1</td>
                            <td colspan="3">
                                Выбранный тип ремня:
                                <br>
                                Клиновой ремень(если поликлиновой ремень &mdash; метку снять)
                            </td>
                            <td>
                                <input type="checkbox" name="remen" checked>
                            </td>
                        </tr>
                        <tr>
                            <td>По таблице 2</td>
                            <td>
                                Стандартный диаметр большого шкива
                            </td>
                            <td>
                                d<sub>2</sub>
                            </td>
                            <td>
                                мм
                            </td>
                            <td>
                                <input type="number" name="d2" />
                            </td>
                        </tr>
                    </table>
                ');
                break;
            case '3':
                return array(
                    'stage' => '3',
                    'title' => 'Определение скорости ремня',
                    'description' => '
                        <p>
                            <img src="img/formula/2.gif" />
                        </p>
                        <p>
                            где V &mdash; скорость ремня, м/с.
                        </p>
                        <p>
                            Скорость ремня не должна превышать [V] = 25 &mdash; 40 м/с
                            (большие значения для узких и поликлиновых ремней, а меньшие &mdash; для нормальных ремней).
                        </p>',
                    'table' => ''
                );
                break;
            default:
                return array(
                    'stage' => '1',
                    'title' => 'Исходные данные',
                    'description' => '<p>В зависимости от формы сечения ремни бывают:
                        плоские, круглые, клиновые и поликлиновые, зубчатые.
                        Наибольшее практическое применение в приводах имеют клиноременные передачи,
                        расчет которых рассмотрен в данной расчетной программе.</p>',
                    'table' => '
                <table>
                        <tr>
                            <td></td>
                            <td>Мощность на ведущем шкиве</td>
                            <td>P<sub>1</sub></td>
                            <td>кВт</td>
                            <td><input type="number" name="P1" /></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Крутящий момент на ведущем шкиве</td>
                            <td>T<sub>1</sub></td>
                            <td>Н*м</td>
                            <td><input type="number" name="T1" /></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Частота вращения ведущего шкива</td>
                            <td>n<sub>1</sub></td>
                            <td>об/мин</td>
                            <td><input type="number" name="n1" /></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Передаточное отношение</td>
                            <td>u</td>
                            <td></td>
                            <td><input type="number" name="u" /></td>
                        </tr>
                        <tr>
                            <td></td>
                            <td>Коэффициент динамичности</td>
                            <td>K<sub>F</sub></td>
                            <td></td>
                            <td><input type="number" name="KF" /></td>
                        </tr>
                    </table>',
                );
        }




    }
}; ?>