<?php class mainView {

    public function renderHeader(){ ?>
        <!DOCTYPE html>
        <html>
            <head>
                <link rel="stylesheet" type="text/css" href="css/common.css" />
                <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css" />

                <script language="JavaScript" src="js/jquery-1.10.2.js"></script>
                <script language="JavaScript" src="js/jquery.validate.min.js"></script>
                <script language="JavaScript" src="js/bootstrap.min.js"></script>
                <script language="JavaScript" src="js/validate-rules.js"></script>

                <title>Расчет ременных передач</title>
            </head>
        <body>
        <section class="container">
            <header>
                <h1>Расчет ременных передач</h1>
            </header>
            <nav class="navbar-fixed-top">
                <ul class="nav">
                    <li>
                        <a type="button" data-toggle="modal" data-target="#tablesModal">Таблицы</a>
                    </li>
                    <li>
                        <a type="button" data-toggle="modal" data-target="#graphicsModal">Графики</a>
                    </li>
                    <li>
                        <a type="button" data-toggle="modal" data-target="#imgModal">Рисунки</a>
                    </li>
                    <li>
                        <a type="button" data-toggle="modal" data-target="#formulaModal">Формулы</a>
                    </li>
                </ul>
            </nav>
            <section id="data-form">
    <?php }


    public function renderFooter(){ ?>
        <!-- Modals -->
        <section class="modal hide fade" id="tablesModal" tabindex="-1" role="dialog" aria-labelledby="tablesModalLabel" aria-hidden="true">
            <header class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="tablesModalLabel">Таблицы</h3>
            </header>
            <section class="modal-body">
                <p>
                    <img src="img/tables/1.jpg" />
                    <img src="img/tables/2.jpg" />
                    <img src="img/tables/3.jpg" />
                    <img src="img/tables/4.jpg" />
                    <img src="img/tables/5.7-5.8.png" />
                </p>
            </section>
        </section>

        <section class="modal hide fade" id="graphicsModal" tabindex="-1" role="dialog" aria-labelledby="graphicsModalLabel" aria-hidden="true">
            <header class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="graphicsModalLabel">Графики</h3>
            </header>
            <section class="modal-body">
                <p>
                    <img src="img/graphics/1.jpg" />
                    <img src="img/graphics/2.jpg" />
                    <img src="img/graphics/3.jpg" />
                </p>
            </section>
        </section>

        <section class="modal hide fade" id="formulaModal" tabindex="-1" role="dialog" aria-labelledby="formulaModalLabel" aria-hidden="true">
            <header class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="formulaModalLabel">Формулы</h3>
            </header>
            <section class="modal-body">
                <p>
                    <img src="img/formula/1.gif" />
                    <img src="img/formula/2.gif" />
                    <img src="img/formula/3.gif" />
                    <img src="img/formula/4.gif" />
                    <img src="img/formula/5.gif" />
                    <img src="img/formula/6.gif" />
                    <img src="img/formula/7.gif" />
                    <img src="img/formula/8.gif" />
                    <img src="img/formula/9.gif" />
                    <img src="img/formula/10.gif" />
                </p>
            </section>
        </section>

        <section class="modal hide fade" id="imgModal" tabindex="-1" role="dialog" aria-labelledby="imgModalLabel" aria-hidden="true">
            <header class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                <h3 id="imgModalLabel">Рисунки</h3>
            </header>
            <section class="modal-body">
                <p>
                    <img src="img/1.png" />
                    Рисунок 1
                </p>
            </section>
        </section>
        </body>
        </html>
    <?php }

} ?>