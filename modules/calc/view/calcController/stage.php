<?php
require_once('/home/u687100369/public_html/modules/calc/view/layout/main.php');

class stageView extends mainView {

    public function render($model){
        $this->renderHeader() ?>

        <form action="/index.php" method="post" id="stage-form">
            <section id="part1">
                <header>
                    <h2>
                        <?php echo $model['title']; ?>
                    </h2>
                </header>
                <article>
                    <?php echo $model['description']; ?>
                </article>

                <?php echo $model['table']; ?>

                <input type="hidden" name="result" value="<?php echo $model['result']?>"/>
                <input type="hidden" name="stage" value="<?php echo $model['stage']?>"/>
                <input type="hidden" name="action" value="stage"/>


                <aside>
                    <button class="previous" name="previous">Назад</button>
                    <button class="next" name="next">Далее</button>
                </aside>
            </section>
        </form>

        <?php $this->renderFooter();
        die();
    }
} ?>