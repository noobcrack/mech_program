jQuery(document).ready(function(){

    if(jQuery('input[name="stage"]').val() == '1'){
        jQuery('.previous').hide();
    };

    var rules = {
        rules: {
                // 1 stage vars
                P1: {
                    required: true,
                    maxlength: 5,
                    number: true
                },
                T1: {
                    required: true,
                    maxlength: 5,
                    number: true
                },
                n1: {
                    required: true,
                    maxlength: 5,
                    number: true
                },
                u: {
                    required: true,
                    maxlength: 5,
                    number: true
                },
                KF: {
                    required: true,
                    maxlength: 5,
                    number: true
                },

                // 2 stage vars
                d1: {
                    required: true,
                    maxlength: 5,
                    number: true
                },
                d2: {
                    required: true,
                    maxlength: 5,
                    number: true
                },
                Ksi: {
                    required: true,
                    maxlength: 5,
                    number: true
                },

                // 3 stage vars
                V: {
                    required: true,
                    maxlength: 5,
                    number: true
                }
            },
        messages: {
            // 1 stage vars
            P1: {
                required: 'Не может быть пустым',
                maxlength: 'Уменьшите количество цифр',
                number: 'Допустимы только целые числа'
            },
            T1: {
                required: 'Не может быть пустым',
                maxlength: 'Уменьшите количество цифр',
                number: 'Допустимы только целые числа'
            },
            n1: {
                required: 'Не может быть пустым',
                maxlength: 'Уменьшите количество цифр',
                number: 'Допустимы только целые числа'
            },
            u: {
                required: 'Не может быть пустым',
                maxlength: 'Уменьшите количество цифр',
                number: 'Допустимы только целые числа'
            },
            KF: {
                required: 'Не может быть пустым',
                maxlength: 'Уменьшите количество цифр',
                number: 'Допустимы только целые числа'
            },

            // 2 stage vars
            d1: {
                required: 'Не может быть пустым',
                maxlength: 'Уменьшите количество цифр',
                number: 'Допустимы только целые числа'
            },
            d2: {
                required: 'Не может быть пустым',
                maxlength: 'Уменьшите количество цифр',
                number: 'Допустимы только целые числа'
            },
            Ksi: {
                required: 'Не может быть пустым',
                maxlength: 'Уменьшите количество цифр',
                number: 'Допустимы только целые числа'
            },

            // 3 stage vars
            V: {
                required: 'Не может быть пустым',
                maxlength: 'Уменьшите количество цифр',
                number: 'Допустимы только целые числа'
            }
        }
    };

    jQuery("#stage-form").validate(rules);
//
//    jQuery("#stage-form").submit(function(){
//    });
//
//    jQuery('.next').click(function(){
//    });

    jQuery('.previous').click(function(){
        history.back();
    });


});


//var active_element_number = 1;
//
//function getNumberElements(){
//    return $("#data-form section").length;
//}
//
//$(function(){   //INIT
//    while((active_element_number + 1) <= getNumberElements())
//    {
//        $("#part" + (active_element_number + 1)).hide();
//        active_element_number++;
//    }
//
//    active_element_number = 1;
//
//    $(".next").click(function()
//    {
//        $("#part" + (active_element_number)).hide();
//        $("#part" + (active_element_number + 1)).show();
//
//        active_element_number++;
//
//        return false;
//    });
//
//    $(".previous").click(function()
//    {
//        $("#part" + (active_element_number)).hide();
//        $("#part" + (active_element_number - 1)).show();
//
//        active_element_number--;
//
//        return false;
//    });
//});

